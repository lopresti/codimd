'use strict'
const { createLogger, format, transports } = require('winston')
const Sentry = require('winston-transport-sentry-node').default

const loggerTransports = [new transports.Console()]

if (process.env.SENTRY_DSN) {
  loggerTransports.push(
    new Sentry({
      sentry: { dsn: process.env.SENTRY_DSN },
      level: 'error'
    })
  )
}

const logger = createLogger({
  level: 'debug',
  format: format.combine(
    format.uncolorize(),
    format.timestamp(),
    format.align(),
    format.splat(),
    format.printf((info) => `${info.timestamp} ${info.level}: ${info.message}`)
  ),
  transports: loggerTransports,
  exitOnError: false
})

logger.stream = {
  write: function (message, encoding) {
    logger.info(message)
  }
}

module.exports = logger
